﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace myapp.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        //public void Execute(System.Web.Routing.RequestContext requestContext)
        //{
        //    var controller = (string)requestContext.RouteData.Values["controller"];
        //    var action = (string)requestContext.RouteData.Values["action"];
        //    requestContext.HttpContext.Response.Write(
        //    string.Format("Controller: {0}, Action: {1}", controller, action));
        //}

        public ActionResult Index()
        {
            int hour = DateTime.Now.Hour;

            ViewBag.Greeting =
             hour < 12
            ? "Good Morning. Time is" + DateTime.Now.ToShortTimeString()
            : "Good Afternoon. Time is " + DateTime.Now.ToShortTimeString();

            return View();
        }
    }
}