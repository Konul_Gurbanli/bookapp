﻿using myapp.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace myapp.Controllers
{
    public class UserController : Controller
    {
        UserDAO u = new UserDAO();

        [HttpPost]
        [ActionName("delete")]
        public ActionResult Delete(int id)
        {
            u.DeleteUser(id);
            return View("Index");
        }

        [HttpGet]
        [ActionName("register")]
        public ActionResult Register()
        {
            return View("Index");
        }

        [HttpPost]
        [ActionName("register")]
        public ActionResult Register(UserModel user) {
            if (ModelState.IsValid)
            {
                u.CreateUser(user);
                int cur_user = u.CheckUser(user.Email, user.Password);
                Session.Add("authorized", true);
                Session.Add("cur_user", cur_user);
                FormsAuthentication.SetAuthCookie(user.Email, false);
                return RedirectToAction("Profile", new { id = cur_user});
            }
            else {
                return View("Index");
            }
            //return View("Index");
        }

        [HttpGet]
        [ActionName("login")]
        public ActionResult Login()
        {
            return View("Index");
        }

        [HttpPost]
        [ActionName("login")]
        public ActionResult Login(string email, string password)
        {
            int res = u.CheckUser(email, password);
            string mes;
            if (res >= 1) {
                mes = "Authorized";
                TempData["authorized"] = true;
                Session.Add("authorized", true);
                Session.Add("cur_user", res);
                TempData["id"] = res;
                FormsAuthentication.SetAuthCookie(email, false);
            }
            else if (res == -1) { mes = "Not authorized"; ViewBag.authorized = false; Session.Add("authorized", false); }
            else { mes = "Couldn't connect"; ViewBag.authorized = false; Session.Add("authorized", false); }
            ViewBag.message = mes;
            //System.Web.Security.FormsAuthentication.RedirectFromLoginPage("");
            //Response.Redirect("Profile/"+res, false);
            return RedirectToAction("Profile", new { id = res });
        }

        [HttpGet]
        [ActionName("profile")]
        public ActionResult GetProfile(int id) {
            UserModel user = u.GetUser(id);
            if (user != null) { ViewBag.user = user;
                List<BookModel> booklist = u.GetUserBooks(id);
                if (booklist != null) { ViewBag.books = booklist; }
                else { ViewBag.books = "not found"; }
            }
            return View("Profile"); 
        }

        [HttpGet]
        [ActionName("edit")]
        public ActionResult editUser(int id) {
            UserModel user = u.GetUser(id);
            ViewBag.user = user;
            return View("EditUser");
        }

        [HttpPost]
        [ActionName("edit")]
        public ActionResult editUser(UserModel  user) {
            if (ModelState.IsValid) {
                u.UpdateUser(user);
            }
            else
            {
                RedirectToAction("edit", new { id=user.Id}); ;
            }
            return RedirectToAction("Profile", new { id = user.Id });
        }

        //[HttpGet]
        //[ActionName("books")]
        //public ActionResult GetUserBooks(int id)
        //{
        //    List<BookModel> booklist = u.GetUserBooks(id);
        //    if (booklist != null) { ViewBag.books = booklist; }
        //    else { ViewBag.books = "not found"; }
        //    return View("Profile");
        //}
    }
}