﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using myapp.Models;
using myapp.Models.Data_Access;
using System.IO;

namespace myapp.Controllers
{
    public class BookController : Controller
    {
        BookDAO b = new BookDAO();
        int COUNT=12;

        [HttpGet]
        [ActionName("select")]
        public ActionResult GetBook(int id)
        {
            BookModel book = b.GetBook(id);
            if (book != null)
            {
                ViewBag.book = book;
            }
            else { ViewBag.book = "Not found"; }
            return View("Book");
        }

        [HttpPost]
        [ActionName("add")]
        public ActionResult addBook([Bind(Exclude ="genre, status, owner, condition, language")]BookModel book, HttpPostedFileBase file) {
            ModelState.Remove("genre");
            ModelState.Remove("status");
            ModelState.Remove("owner"); 
            if (ModelState.IsValid)
            {
                    var x = file.ContentType;
                    if (file.ContentLength > 0 && (file.ContentType.Equals("image/jpg")||file.ContentType.Equals("image/png") || file.ContentType.Equals("image/svg") || 
                        file.ContentType.Equals("image/jpeg")))
                    {
                        var fileType = Path.GetExtension(file.FileName);
                        var guid = Guid.NewGuid();
                        var fileName = guid + fileType;
                        var path = Path.Combine(Server.MapPath("~/Content/images/books"), fileName);
                        file.SaveAs(path);
                        Image i = new Image();
                        i.category = "book";
                        i.name = guid.ToString();
                        i.path = "~/Content/images/books/"+fileName;
                        i.type = fileType;
                        i.size = file.ContentLength.ToString();
                        b.add_img(i);
                        i.id = b.get_image(i.name);
                        book.img = i;
                        ViewBag.Message = "Upload successful";
                    }
                    else
                    { 
                    ViewBag.Message = "Upload failed";
                    }
                    b.add_book(book);
                return RedirectToAction("Profile", "User", new { id= book.owner_id});
            }
            return RedirectToAction("add");
        }

        [HttpGet]
        [ActionName("add")]
        public ActionResult addBook() { 
            var dbgenres = Lookup.getGenres();
            var dblangs = Lookup.getLanguages();
            var dbstats = Lookup.getStatuses();
            var dbconds = Lookup.getBookConditions();
            List<SelectListItem> genres = new List<SelectListItem>();
            foreach (var g in dbgenres)
            {
                genres.Add(new SelectListItem
                {
                    Text = g.genre_name,
                    Value = g.genre_id.ToString()
                });
            }
            List<SelectListItem> langs = new List<SelectListItem>();
            foreach (var l in dblangs)
            {
                langs.Add(new SelectListItem
                {
                    Text = l.lang,
                    Value = l.lang_id.ToString()
                });

            }
            List<SelectListItem> stats = new List<SelectListItem>();
            foreach (var s in dbstats)
            {
                stats.Add(new SelectListItem
                {
                    Text = s.stat,
                    Value = s.stat_id.ToString()
                });

            }

            List<SelectListItem> conds = new List<SelectListItem>();
            foreach (var c in dbconds)
            {
                conds.Add(new SelectListItem
                {
                    Text = c.cond,
                    Value = c.cond_id.ToString()
                });

            }
            ViewBag.genres = genres;
            ViewBag.langs = langs;
            ViewBag.conds = conds;
            ViewBag.stats = stats;
            return View("AddBook");
        }

        [HttpGet]
        [ActionName("all_c")]
        public ActionResult allBooks(int start, int count) {
            List<BookModel> books = b.get_books(start-1, count, 0,0,0,0,0);
            ViewBag.all = b.count_books();
            ViewBag.start = start;
            ViewBag.count = count;
            var dbgenres = Lookup.getGenres();
            var dblangs = Lookup.getLanguages();
            ViewBag.genres = dbgenres;
            ViewBag.langs = dblangs;
            if (books != null) {
                ViewBag.books = books;
            }
            else { ViewBag.book = "No books exist"; }
            return View("AllBooks");
        }

        [HttpGet]
        [ActionName("all")]
        public ActionResult allBooks()
        {
            int start = 1;
            List<BookModel> books = b.get_books(start-1,COUNT, 0, 0, 0, 0, 0);
            var dbgenres = Lookup.getGenres();
            var dblangs = Lookup.getLanguages();
            ViewBag.genres = dbgenres;
            ViewBag.langs = dblangs;
            ViewBag.all = b.count_books();
            ViewBag.start = start;
            ViewBag.count = COUNT;
            if (books != null)
            {
                
                ViewBag.books = books;
            }
            else { ViewBag.book = "No books exist"; }
            return View("AllBooks");
        }

        [HttpPost]
        [ActionName("filter")]
        public ActionResult filter(int genre, int lang,int cond, int rate, int stat ) {
            int start = 1;
            List<BookModel> books = b.get_books(start - 1, COUNT, genre, lang, cond, rate, stat);
            System.Diagnostics.Debug.WriteLine(books.ToString());
            var dbgenres = Lookup.getGenres();
            var dblangs = Lookup.getLanguages();
            ViewBag.genres = dbgenres;
            ViewBag.langs = dblangs;
            ViewBag.all = b.count_filtered_books(genre,lang,cond,rate, stat);
            ViewBag.start = start;
            ViewBag.count = COUNT;
            if (books != null)
            {

                ViewBag.books = books;
            }
            else { ViewBag.book = "No books exist"; }
            return View("AllBooks");
        }

        //[HttpGet]
        //[ActionName("test")]
        //public int count() {
        //    int c=b.count_books();
        //    return c;
        //}
    }
}