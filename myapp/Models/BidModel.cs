﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace myapp.Models
{
    public class BidModel
    {
        [Required]
        [DisplayName("Bid id")]
        public int id { get; set; }
        [Required]
        [DisplayName("Bid owner")]
        public UserModel user { get; set; }
        [Required]
        [DisplayName("Target book")]
        public BookModel book { get; set; }
        [DisplayName("Bid text")]
        public string text { get; set; }
        [Required]
        [DisplayName("Bid type")]
        public int bid_type { get; set; }
        [DisplayName("Bid owner's book")]
        public BookModel bid_book { get; set; }
        [DisplayName("Bid owner's amount")]
        public float bid_amount { get; set; }
        [Required]
        [DisplayName("Bid status")]
        public int status { get; set; }
    }
}