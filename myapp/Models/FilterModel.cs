﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myapp.Models
{
    public class FilterModel
    {
        public bool test { get; set; }
        public int[] genre { get; set; }
        public int[] lang{ get; set; }
        public int[] cond { get; set; }
        public int rate{ get; set; }
        public int[] stat { get; set; }

        public FilterModel() {
            this.genre[0] = 0;
            this.lang[0] = 0;
            this.cond[0] = 0;
            this.rate = -1;
            this.stat[0] = 0;
        }
    }
}