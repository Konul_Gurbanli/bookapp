﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myapp.Models
{
    public class BookCondition
    {
        public int cond_id { get; set; }
        public string cond { get; set; }

        public BookCondition(int id, string name) {
            this.cond_id=id;
            this.cond = name;
        }
    }
}