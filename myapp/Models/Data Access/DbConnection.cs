﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace myapp.Models
{
    public class DbConnection
    {
       
            private DbConnection()
            {
            }

            private string databaseName = "bookapp";
            public string DatabaseName
            {
                get { return databaseName; }
                set { databaseName = value; }
            }

            public string Password { get; set; }
            private MySqlConnection connection = null;

            public MySqlConnection Connection
            {
                get { return connection; }
            }

            private static DbConnection _instance = null;
            public static DbConnection Instance()
            {
                if (_instance == null)
                    _instance = new DbConnection();
                return _instance;
            }

            public bool IsConnect()
            {
                bool result = true;
                if (true)
                {
                    if (String.IsNullOrEmpty(databaseName))
                        result = false;
                    string connstring = string.Format("Server=localhost; database={0}; UID=root; password=asdfg123; Allow Zero Datetime=true", databaseName);
                    connection = new MySqlConnection(connstring);
                    connection.Open();
                    result = true;
                }

                return result;
            }

            public void Close()
            {
                connection.Close();
            }
        }
}