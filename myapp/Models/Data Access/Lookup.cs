﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;


namespace myapp.Models
{
    public class Lookup
    {
        public static List< Genre> genreList;
        public static List< BidType> typeList;
        public static List< Status> statusList;
        public static List< Language> langList;
        public static List< BookCondition> condList;

        public static List<Genre> getGenres() {
            List< Genre> genres = new List< Genre>();
            string proc = "get_genres";
            MySqlDataReader reader = null;
            DbConnection conn = DbConnection.Instance();
            if (conn.IsConnect()) {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                reader = cmd.ExecuteReader();
                while (reader.Read()){
                    int id = int.Parse(reader["genre_id"].ToString());
                    string name = reader["genre_name"].ToString();
                    genres.Add(new Genre(id, name));
                }
                genreList = genres;
                reader.Close();
                conn.Close();
            }
            return genreList;
        }

        public static List< BidType> getBidTypes()
        {
            List <BidType> types= new List < BidType>();
            string proc = "get_bid_types";
            MySqlDataReader reader = null;
            DbConnection conn = DbConnection.Instance();
            if (conn.IsConnect())
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = int.Parse(reader["type_id"].ToString());
                    string name = reader["type"].ToString();
                    types.Add(new BidType(id,name));
                }
               typeList = types;
               reader.Close();
               conn.Close();
            }
            return typeList;
        }

        public static List<BookCondition> getBookConditions()
        {
            List< BookCondition> conds = new List< BookCondition>();
            string proc = "get_book_conditions";
            MySqlDataReader reader = null;
            DbConnection conn = DbConnection.Instance();
            if (conn.IsConnect())
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = int.Parse(reader["cond_id"].ToString());
                    string name = reader["cond"].ToString();
                    conds.Add(new BookCondition(id, name));
                }
                condList = conds;
                reader.Close();
                conn.Close();
            }
            return condList;
        }

         public static List< Status> getStatuses()
        {
            List< Status> stats = new List <Status>();
            string proc = "get_statuses";
            MySqlDataReader reader = null;
            DbConnection conn = DbConnection.Instance();
            if (conn.IsConnect())
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = int.Parse(reader["stat_id"].ToString());
                    string name = reader["stat"].ToString();
                    stats.Add( new Status(id, name));
                }
                statusList = stats;
                reader.Close();
                conn.Close();
            }
            return statusList;
        }


        public static List <Language> getLanguages()
        {
            List <Language> langs = new List<Language>();
            string proc = "get_languages";
            MySqlDataReader reader = null;
            DbConnection conn = DbConnection.Instance();
            if (conn.IsConnect())
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = int.Parse(reader["lang_id"].ToString());
                    string name = reader["lang"].ToString();
                    langs.Add(new Language(id, name));
                }
                langList= langs;
                reader.Close();
                conn.Close();
            }
            return langList;
        }

    }
}