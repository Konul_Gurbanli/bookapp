﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using myapp.Models.Data_Access;

namespace myapp.Models.Data_Access
{
    public class BookDAO
    {
        UserDAO ud = new UserDAO();

        //get the book from database with the given id
        public BookModel GetBook(int id) {
            DbConnection conn = DbConnection.Instance();
            MySqlDataReader reader = null;
            BookModel book = new BookModel();
            string proc = "get_book_by_id";
            //UserDAO ud = new UserDAO();
            if (conn.IsConnect()) {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                var p1 = cmd.Parameters.Add("id", MySqlDbType.Int32);
                p1.Direction = System.Data.ParameterDirection.Input;
                p1.Value = id;
                reader = cmd.ExecuteReader();
                while (reader.Read()) {
                    book.id = id;
                    book.author = reader["author"].ToString();
                    book.title = reader["title"].ToString();
                    int gid = int.Parse(reader["genre_id"].ToString());
                    book.genre = new Genre(gid, reader["genre_name"].ToString());
                    int st = int.Parse(reader["status"].ToString());
                    book.status = new Status(st, reader["stat"].ToString());
                    int uid = int.Parse(reader["owner"].ToString());
                    book.owner = ud.GetUser(uid);
                    if (reader["condition_id"] != null)
                    {
                        int cd = int.Parse(reader["condition_id"].ToString());
                        book.condition = new BookCondition(cd, reader["cond"].ToString());
                    }
                    if (reader["language"] != null)
                    {
                        int ln = int.Parse(reader["language"].ToString());
                        book.language = new Language(ln, reader["lang"].ToString());
                    }
                    if (reader["rate"] != null)
                    {
                        float rt = float.Parse(reader["rate"].ToString());
                        book.rate = rt;
                    }
                    if (reader["pages"] != null)
                    {
                        int pg = int.Parse(reader["pages"].ToString());
                        book.pages = pg;
                    }
                    if (reader["published"] != null)
                    {
                        //var p = reader["published"];
                        string published = reader["published"].ToString();
                        DateTime d;
                        if (DateTime.TryParse(published, out d)) { book.published = d.Date; }
                        else { book.published = new DateTime(int.Parse(published.Substring(4)), 1, 1); }
                    }
                    if (reader["summary"] != null)
                    {
                        book.summary = reader["summary"].ToString();
                    }
                    if (reader["img"]!=null) {
                        int img = int.Parse(reader["img"].ToString());
                        book.img = new Image();
                        book.img.id = img;
                        book.img.path = reader["path"].ToString();
                    }
                }
                reader.Close();
                conn.Close();
            }
            return book;
        }


        //get the image id using its unique name
        public int get_image(string name)
        {
            DbConnection conn = DbConnection.Instance();
            MySqlDataReader reader = null;
            //Image i = new Image();
            int i=0;
            string proc = "get_image";
            if (conn.IsConnect()) {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                var p1 = cmd.Parameters.Add("name_", MySqlDbType.VarChar, 36);
                p1.Direction = System.Data.ParameterDirection.Input;
                p1.Value = name;
                reader = cmd.ExecuteReader();
                while (reader.Read()) {
                    i= int.Parse(reader["id"].ToString());
                }
            }
            return i;
        }

        //add a new book to the database
        public void add_book(BookModel book) {
            DbConnection conn = DbConnection.Instance();
            string proc = "add_book";
            if (conn.IsConnect()) {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                cmd.Parameters.Add("author_", MySqlDbType.VarChar, 100).Value =book.author;
                cmd.Parameters.Add("title_", MySqlDbType.VarChar, 500).Value = book.title;
                cmd.Parameters.Add("genre_id_", MySqlDbType.Int16).Value = book.genre_id;
                cmd.Parameters.Add("published_", MySqlDbType.Date).Value = book.published;
                cmd.Parameters.Add("language_", MySqlDbType.Int16).Value = book.lang_id;
                cmd.Parameters.Add("condition_id_", MySqlDbType.Int16).Value=book.cond_id;
                cmd.Parameters.Add("pages_", MySqlDbType.Int16).Value = book.pages;
                cmd.Parameters.Add("summary_", MySqlDbType.VarChar, 5000).Value = book.summary;
                cmd.Parameters.Add("rate_", MySqlDbType.Float).Value = book.rate;
                cmd.Parameters.Add("status_", MySqlDbType.Int16).Value = book.stat_id;
                cmd.Parameters.Add("owner_", MySqlDbType.Int16).Value = book.owner_id;
                cmd.Parameters.Add("img_", MySqlDbType.Int16).Value = book.img.id;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //return the count of the books that is in the database
        public int count_books() {
            int count =0;
            DbConnection conn = DbConnection.Instance();
            MySqlDataReader reader = null;
            string proc="count_books";
            if (conn.IsConnect()) {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                reader=cmd.ExecuteReader();
                while (reader.Read()) {
                    count = int.Parse(reader["count(*)"].ToString());
                }
                reader.Close();
                conn.Close();
            }
            return count;
        }

        public int count_filtered_books(int genre, int lang, int cond, int rate, int stat) {
            int count = 0;
            DbConnection conn = DbConnection.Instance();
            MySqlDataReader reader = null;
            string proc = "count_filtered_books";
            if (conn.IsConnect())
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                var p1 = cmd.Parameters.Add("genre_id_", MySqlDbType.Int32);
                p1.Direction = System.Data.ParameterDirection.Input;
                p1.Value = genre;
                var p2 = cmd.Parameters.Add("lang_id_", MySqlDbType.Int32);
                p2.Direction = System.Data.ParameterDirection.Input;
                p2.Value = lang;
                var p3 = cmd.Parameters.Add("cond_id_", MySqlDbType.Int32);
                p3.Direction = System.Data.ParameterDirection.Input;
                p3.Value = cond;
                var p4 = cmd.Parameters.Add("rate_", MySqlDbType.Int32);
                p4.Direction = System.Data.ParameterDirection.Input;
                p4.Value = rate;
                var p5 = cmd.Parameters.Add("stat_id_", MySqlDbType.Int32);
                p5.Direction = System.Data.ParameterDirection.Input;
                p5.Value = stat;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    count = int.Parse(reader["count(*)"].ToString());
                }
                reader.Close();
                conn.Close();
            }
            return count;
        }

        //add image to the img table
        public void add_img(Image i) {
            DbConnection conn = DbConnection.Instance();
            string proc = "add_image";
            if (conn.IsConnect()) {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                cmd.Parameters.Add("name_", MySqlDbType.VarChar, 36).Value = i.name;
                cmd.Parameters.Add("type_", MySqlDbType.VarChar, 25).Value = i.type;
                cmd.Parameters.Add("path_", MySqlDbType.VarChar, 300).Value = i.path;
                cmd.Parameters.Add("category_", MySqlDbType.VarChar, 25).Value = i.category;
                cmd.Parameters.Add("size_", MySqlDbType.VarChar, 25).Value = i.size;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //get a list of all the books in the database and in case of additional parameters, filter them out
        public List<BookModel> get_books(int start_row, int count, int genre_id, int lang_id, int cond_id, int rate, int stat_id) {
            DbConnection conn = DbConnection.Instance();
            MySqlDataReader reader = null;
            List<BookModel> blist = new List<BookModel>();
            string proc = "get_books";
            if (conn.IsConnect()) {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                var p1 = cmd.Parameters.Add("start_row", MySqlDbType.Int32);
                p1.Direction = System.Data.ParameterDirection.Input;
                p1.Value = start_row;
                var p2 = cmd.Parameters.Add("count", MySqlDbType.Int32);
                p2.Direction = System.Data.ParameterDirection.Input;
                p2.Value = count;
                var p3 = cmd.Parameters.Add("genre_id_", MySqlDbType.Int32);
                p3.Direction = System.Data.ParameterDirection.Input;
                p3.Value = genre_id;
                var p4 = cmd.Parameters.Add("lang_id_", MySqlDbType.Int32);
                p4.Direction = System.Data.ParameterDirection.Input;
                p4.Value = lang_id;
                var p5 = cmd.Parameters.Add("cond_id_", MySqlDbType.Int32);
                p5.Direction = System.Data.ParameterDirection.Input;
                p5.Value = cond_id;
                var p6 = cmd.Parameters.Add("rate_", MySqlDbType.Int32);
                p6.Direction = System.Data.ParameterDirection.Input;
                p6.Value = rate;
                var p7 = cmd.Parameters.Add("stat_id_", MySqlDbType.Int32);
                p7.Direction = System.Data.ParameterDirection.Input;
                p7.Value = stat_id;
                reader = cmd.ExecuteReader();
                while (reader.Read()) {
                    BookModel b = new BookModel();
                    b.id= int.Parse(reader["book_id"].ToString());
                    b.author = reader["author"].ToString();
                    b.title = reader["title"].ToString();
                    int gid= int.Parse(reader["genre_id"].ToString());
                    b.genre = new Genre(gid, reader["genre_name"].ToString() );
                    int sid = int.Parse(reader["status"].ToString());
                    b.status = new Status(sid, reader["stat"].ToString());
                    int uid = int.Parse(reader["owner"].ToString());
                    b.owner=ud.GetUser(uid);
                    if (reader["condition_id"] != null)
                    {
                        int cd = int.Parse(reader["condition_id"].ToString());
                        b.condition = new BookCondition(cd, reader["cond"].ToString());
                    }
                    if (reader["language"] != null)
                    {
                        int ln = int.Parse(reader["language"].ToString());
                        b.language = new Language(ln, reader["lang"].ToString());
                    }
                    if (reader["rate"] != null)
                    {
                        float rt = float.Parse(reader["rate"].ToString());
                        b.rate = rt;
                    }
                    if (reader["pages"] != null)
                    {
                        int pg = int.Parse(reader["pages"].ToString());
                        b.pages = pg;
                    }
                    if (reader["published"] != null)
                    {
                        //var p = reader["published"];
                        string published = reader["published"].ToString();
                        DateTime d;
                        if (DateTime.TryParse(published, out d)) { b.published = d.Date; }
                        else { b.published = new DateTime(int.Parse(published.Substring(4)), 1, 1); }
                    }
                    if (reader["summary"] != null)
                    {
                        b.summary = reader["summary"].ToString();
                    }
                    if (reader["img"] != null)
                    {
                        int img = int.Parse(reader["img"].ToString());
                        b.img = new Image();
                        b.img.id = img;
                        b.img.path = reader["path"].ToString();
                    }
                    blist.Add(b);
                }
                reader.Close();
                conn.Close();
            }
            return blist;
        }
    }
}