﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myapp.Models
{
    public class UserDAO
    {
        public List<UserModel> UserList = new List<UserModel>();

        //check id exists
        public int CheckUser(string email, string password) {
            DbConnection conn = DbConnection.Instance();
            string query = "select check_user(\""+email+"\",\""+password+"\") from users";
            int res=0;
            if (conn.IsConnect())
            {
                //conn.Connection.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn.Connection);
                res=int.Parse(cmd.ExecuteScalar()+"");
                //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //cmd.CommandText = query;
                //cmd.Connection = conn.Connection;
                //var p1=cmd.Parameters.Add("_email", MySqlDbType.String);
                //p1.Direction = System.Data.ParameterDirection.Input;
                //p1.Value = email;
                //var p2 = cmd.Parameters.Add("_password", MySqlDbType.String);
                //p2.Direction = System.Data.ParameterDirection.Input;
                //p2.Value = password;
                //var ret = cmd.Parameters.Add("user_found", MySqlDbType.Int32);
                //ret.Direction = System.Data.ParameterDirection.ReturnValue; 
                
                //System.Diagnostics.Debug.WriteLine("RESULT IS: " + res);

                conn.Close();
            }
            return res;
        }


        //user by id
        public UserModel GetUser(int id)
        {
            UserModel user = new UserModel();
            DbConnection conn = DbConnection.Instance();
            string proc = "get_user_by_id";
            MySqlDataReader reader = null;
            if (conn.IsConnect())
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                var p1 = cmd.Parameters.Add("id", MySqlDbType.Int32);
                p1.Direction = System.Data.ParameterDirection.Input;
                p1.Value = id;
                reader = cmd.ExecuteReader();
                

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        user.Id = id;
                        user.FirstName = reader.GetString(1);
                        user.LastName = reader.GetString(2);
                        user.Email = reader.GetString(3);
                        user.Password = reader.GetString(4);
                        var fc = reader.FieldCount;
                        if (reader["address"]!=null)
                        {
                            user.Address = reader["address"].ToString();
                        }
                        if (reader["phone"] != null) {
                            user.Phone = reader["phone"].ToString();
                        }
                    }
                    reader.Close();
                }
                else
                {
                    Console.WriteLine("No user found!");
                }
                conn.Close();
            }
            return user;
        }

        //create
        public void CreateUser(UserModel userModel) {
            DbConnection conn = DbConnection.Instance();
            string query = "call create_user(\"" + userModel.FirstName + "\",\"" + userModel.LastName+"\",\""
                +userModel.Email+"\",\""+userModel.Password+"\",\""+userModel.Address+"\",\""+userModel.Phone+"\")";
            if (conn.IsConnect())
            {
                MySqlCommand cmd = new MySqlCommand(query, conn.Connection);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //update
        public void UpdateUser(UserModel userModel) {
            DbConnection conn = DbConnection.Instance();
            string query = "call update_user("+userModel.Id+",\"" + userModel.FirstName + "\",\"" + userModel.LastName + "\",\""
                + userModel.Email + "\",\"" + userModel.Password + "\",\"" + userModel.Address + "\",\"" + userModel.Phone + "\")";
            if (conn.IsConnect()) {
                MySqlCommand cmd = new MySqlCommand(query, conn.Connection);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //delete
        public void DeleteUser(int id) {
            DbConnection conn = DbConnection.Instance();
            string query = "call delete_user(" + id+ ")";
            if (conn.IsConnect())
            {
                MySqlCommand cmd = new MySqlCommand(query, conn.Connection);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //get user's books

        public List<BookModel> GetUserBooks(int id) {
            DbConnection conn = DbConnection.Instance();
            string proc = "get_user_books";
            MySqlDataReader reader = null;
            List<BookModel> books = new List<BookModel>();
            if (conn.IsConnect()) {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = proc;
                cmd.Connection = conn.Connection;
                var p1 = cmd.Parameters.Add("user_id", MySqlDbType.Int32);
                p1.Direction = System.Data.ParameterDirection.Input;
                p1.Value = id;
                reader = cmd.ExecuteReader();
                while (reader.Read()) {
                    BookModel book = new BookModel();
                    book.id = int.Parse(reader["book_id"].ToString());
                    book.author = reader["author"].ToString();
                    book.title = reader["title"].ToString();
                    int gid = int.Parse(reader["genre_id"].ToString());
                    book.genre = new Genre(gid, reader["genre_name"].ToString());
                    int st = int.Parse(reader["status"].ToString());
                    book.status = new Status(st, reader["stat"].ToString());
                    book.owner = GetUser(id);
                    if(reader["condition_id"] != null) {
                        int cd = int.Parse(reader["condition_id"].ToString());
                        book.condition = new BookCondition(cd, reader["cond"].ToString());
                    }
                    if (reader["language"] != null)
                    {
                        int ln = int.Parse(reader["language"].ToString());
                        book.language = new Language(ln, reader["lang"].ToString());
                    }
                    if (reader["rate"] != null)
                    {
                        float rt = float.Parse(reader["rate"].ToString());
                        book.rate=rt;
                    }
                    if (reader["pages"] != null)
                    {
                        int pg = int.Parse(reader["pages"].ToString());
                        book.pages=pg;
                    }
                    if (reader["published"] != null)
                    {
                        DateTime d;
                        DateTime.TryParse(reader["published"].ToString(), out d);
                        book.published = d;
                    }
                    if (reader["summary"] != null)
                    {
                        book.summary = reader["summary"].ToString();
                    }
                    books.Add(book);
                }
                reader.Close();
                conn.Close();
            }

            return books;
        }

    }
}