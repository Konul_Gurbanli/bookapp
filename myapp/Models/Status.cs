﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myapp.Models
{
    public class Status
    { 
        
        public int stat_id;
        public string stat;

        public Status(int id, string name)
        {
            this.stat_id = id;
            this.stat = name;
        }
    }
}