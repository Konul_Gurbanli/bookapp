﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace myapp.Models
{
    public class BookModel
    {
        [Required]
        [DisplayName("Book Id")]
        public int id { get; set; }
        [Required]
        [DisplayName("Author")]
        public string author { get; set; }
        [Required]
        [DisplayName("Book Title")]
        public string title { get; set; }
        [Required]
        [DisplayName("Book Genre")]
        public Genre genre { get; set; }
        [Required]
        [DisplayName("Status")]
        public Status status { get; set; }
        [Required]
        [DisplayName("Book owner")]
        public UserModel owner { get; set; }
        public DateTime published { get;  set; }
        public Language language { get; set; }
        public BookCondition condition { get; set; }
        public int pages { get; set; }
        public string summary { get; set; }
        public float rate { get; set; }
        public Image img{ get; set; }
        public int genre_id{ get; set; }
        public int stat_id { get; set; }
        public int owner_id { get; set; }
        public int lang_id{ get; set; }
        public int cond_id{ get; set; }
        public FilterModel filter { get; set; }
    }
}