﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myapp.Models
{
    public class Image
    {
        public int id { get; set; }
        public string name{ get; set; }
        public string type { get; set; }
        public string size { get; set; }
        public string path{ get; set; }
        public string category{ get; set; }

        public Image() { }


    }
}