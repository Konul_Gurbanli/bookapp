﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myapp.Models
{
    public class Genre
    {
        public int genre_id { get; set; }
        public string genre_name { get; set; }

        public Genre(int id, string name) {
            this.genre_id = id;
            this.genre_name = name;
        }
    }
}