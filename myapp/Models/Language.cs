﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myapp.Models
{
    public class Language
    {
        public int lang_id { get; set; }
        public string lang { get; set; }

        public Language(int id, string name)
        {
            this.lang_id = id;
            this.lang = name;
        }
    }
}