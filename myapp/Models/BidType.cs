﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myapp.Models
{
    public class BidType
    {
        public int type_id { get; set; }
        public string type { get; set; }

        public BidType(int id, string name)
        {
            this.type_id = id;
            this.type = name;
        }
    }
}