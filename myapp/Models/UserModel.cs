﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
//using System.Web.Mvc.Html;
using System.ComponentModel;

namespace myapp.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        [DisplayName("First Name")]
        [Required(ErrorMessage ="First name is required!")]
        public String FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = "Last name is required!")]
        public String LastName { get; set; }

        [Required (ErrorMessage ="Email is required!")]
        [StringLength(50)]
        public String Email { get; set; }

        [Required(ErrorMessage ="Password is required!")]
        public String Password { get; set; }

        public String Address { get; set; }

        public String Phone { get; set; }
    }
}