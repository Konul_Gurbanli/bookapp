create table users (
user_id int not null auto_increment,
first_name varchar(100) not null,
last_name varchar(100) not null,
email varchar(50) not null,
password varchar(50) not null,
address varchar(500),
phone int,
primary key (user_id)
);


create table genre (
genre_id int not null auto_increment,
genre_name varchar (50) not null,
primary key(genre_id)
);

insert into genre (genre_name) values ('science');

create  table book_condition (
cond_id int not null auto_increment,
cond varchar(50) not null,
primary key (cond_id)
);

insert into book_condition (cond) values('new');
insert into book_condition (cond) values('used-like-new');
insert into book_condition (cond) values('used-very-good');
insert into book_condition (cond) values('used-good');
insert into book_condition (cond) values('used-acceptable');

select * from book_condition;

create table status (
stat_id int not null auto_increment,
stat varchar(20) not null,
primary key(stat_id)
);

insert into status (stat) values('active');
insert into status (stat) values ('closed');

select * from status;

create table books (
book_id int not null auto_increment,
author varchar(100) not null,
title varchar(500) not null,
genre_id int not null,
published date,
language varchar(50),
condition_id int,
pages int,
summary varchar(5000),
rate int,
primary key(book_id),
foreign key(genre_id) references genre(genre_id),
foreign key(condition_id) references book_condition(cond_id)
);

create table bid_type(
type_id int not null auto_increment,
type varchar (10) not null,
primary key (type_id)
); 

insert into bid_type (type) values ('book');
insert into bid_type (type) values ('cash');

select * from bid_type;

create table bid (
bid_id int not null auto_increment,
bid_user_id int not null,
book_id int not null,
bid_text varchar(1000),
bid_type int not null,
bid_book_id int,
bid_amount int,
bid_status int not null default 1,
primary key (bid_id),
foreign key (bid_user_id) references users(user_id),
foreign key (book_id) references books(book_id),
foreign key (bid_type) references bid_type(type_id),
foreign key (bid_book_id) references books(book_id),
foreign key (bid_status) references status(stat_id)
);

alter table bid modify column bid_amount float;

alter table books add status int not null default 1; 
alter table books add constraint foreign key(status) references status(stat_id);
alter table books modify column rate float; 
select * from books;

/*str_to_date('2001-00-00', '%y-%m-%d')*/
insert into books (owner, author, title, genre_id, published, language, condition_id, pages, summary, rate, status) 
values (1,'Stephen Hawking', 'The Universe in a nutshell', 1, '2001-00-00', 1, 1, 224, 'A book about the universe adnd how it works', 5, 1);

select * from books;

update books set book_id=1 where book_id=2;

create table language(
lang_id int not null auto_increment,
lang varchar(30) not null,
primary key(lang_id)
);

insert into language (lang) values ('English');

delete from books where book_id=1;

alter table books modify column language int;
alter table books add constraint foreign key(language) references language(lang_id);

alter table books add column owner int not null;
alter table books add constraint foreign key(owner) references users(user_id);

alter table users modify column phone varchar(30);

call create_user('Konul', 'Gurbanli', 'konul.qurbanli@gmail.com', 'asdfg123', 'M.Sharifli 1p', '+994513551570');

select * from users;

call update_user(1,'Konul', 'Gurbanli', 'konul.qurbanli@gmail.com', 'asdfg123', 'M.Sharifli 1p', '+994513551570');

call create_user('Seva', 'Jafarli', 'sjafarli2019@ada.edu.az', 'seva', 'Razin', '+994555313868');
call create_user('Javad', 'Compiler', 'javad@comp.az', 'java', 'Oracle', '+123');

call delete_user(9);

select check_user('konul.qurbanli@gmail.com', 'asdfg123') from users;

call create_user('Javad', 'Compiler', 'javad@comp.az', 'java', null, null);

call get_user_by_id(3);

call get_all_books();

delete from books where book_id=3;

call get_user_books(1);

call get_genres();
call get_bid_types();
call get_book_conditions();
call get_statuses();
call get_languages();